﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carlos.Entities
{
    [Validator(typeof(UserValidator))]
    public partial class User
    {
    }

    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Name cannot be blank.")
                .Length(0, 256).WithMessage("Name cannot be more than 256 characters.");

            RuleFor(x => x.Birthdate).NotEmpty().WithMessage("Birthdate cannot be blank.")
                .LessThan(DateTime.Today).WithMessage("You cannot enter a birthdate in the future.");
        }
    }
}
