﻿
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.SqlServer.Types;
using System.Runtime.Serialization;
using System.ComponentModel;
using inercya.EntityLite;	
using inercya.EntityLite.Extensions;		

namespace Carlos.Entities
{
	[Serializable]
	[DataContract]
	[SqlEntity(BaseTableName="users")]
	public partial class User
	{
		private Int32 _id;
		[DataMember]
		[SqlField(DbType.Int32, 4, Precision = 10, IsKey=true, IsAutoincrement=true, IsReadOnly = true, ColumnName ="Id", BaseColumnName ="Id", BaseTableName = "users" )]
		public Int32 Id 
		{ 
		    get { return _id; } 
			set 
			{
			    _id = value;
			}
        }

		private String _name;
		[DataMember]
		[SqlField(DbType.AnsiString, 256, ColumnName ="Name", BaseColumnName ="Name", BaseTableName = "users" )]
		public String Name 
		{ 
		    get { return _name; } 
			set 
			{
			    _name = value;
			}
        }

		private DateTime _birthdate;
		[DataMember]
		[SqlField(DbType.Date, 3, ColumnName ="Birthdate", BaseColumnName ="Birthdate", BaseTableName = "users" )]
		public DateTime Birthdate 
		{ 
		    get { return _birthdate; } 
			set 
			{
			    _birthdate = value;
			}
        }


	}

	public partial class UserRepository : Repository<User> 
	{
		public UserRepository(DataService DataService) : base(DataService)
		{
		}

		public new CarlosDataService  DataService  
		{
			get { return (CarlosDataService) base.DataService; }
			set { base.DataService = value; }
		}

		public User Get(string projectionName, System.Int32 id)
		{
			return ((IRepository<User>)this).Get(projectionName, id, FetchMode.UseIdentityMap);
		}

		public User Get(string projectionName, System.Int32 id, FetchMode fetchMode = FetchMode.UseIdentityMap)
		{
			return ((IRepository<User>)this).Get(projectionName, id, fetchMode);
		}

		public User Get(Projection projection, System.Int32 id)
		{
			return ((IRepository<User>)this).Get(projection, id, FetchMode.UseIdentityMap);
		}

		public User Get(Projection projection, System.Int32 id, FetchMode fetchMode = FetchMode.UseIdentityMap)
		{
			return ((IRepository<User>)this).Get(projection, id, fetchMode);
		}

		public User Get(string projectionName, System.Int32 id, params string[] fields)
		{
			return ((IRepository<User>)this).Get(projectionName, id, fields);
		}

		public User Get(Projection projection, System.Int32 id, params string[] fields)
		{
			return ((IRepository<User>)this).Get(projection, id, fields);
		}

		public bool Delete(System.Int32 id)
		{
			var entity = new User { Id = id };
			return this.Delete(entity);
		}
	}

	public static partial class UserFields
	{
		public const string Id = "Id";
		public const string Name = "Name";
		public const string Birthdate = "Birthdate";
	}

}

namespace Carlos.Entities
{
	public partial class CarlosDataService : DataService
	{
		partial void OnCreated();

		private void Init()
		{
			EntityNameToEntityViewTransform = TextTransform.None;
			EntityLiteProvider.DefaultSchema = "dbo";
			OnCreated();
		}

        public CarlosDataService() : base("CarlosDB")
        {
			Init();
        }

        public CarlosDataService(string connectionStringName) : base(connectionStringName)
        {
			Init();
        }

        public CarlosDataService(string connectionString, string providerName) : base(connectionString, providerName)
        {
			Init();
        }

		private Carlos.Entities.UserRepository _UserRepository;
		public Carlos.Entities.UserRepository UserRepository
		{
			get 
			{
				if ( _UserRepository == null)
				{
					_UserRepository = new Carlos.Entities.UserRepository(this);
				}
				return _UserRepository;
			}
		}
	}
}
