﻿using Carlos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Results;

namespace Prueba_Carlos.Controllers
{
    public class BaseApiController : ApiController
    {
        static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        public CarlosDataService DataService { get; set; }

        public ResponseMessageResult NotModified(Guid etag)
        {
            var response = new HttpResponseMessage(HttpStatusCode.NotModified);
            response.Headers.CacheControl = new CacheControlHeaderValue { NoCache = true };
            response.Headers.ETag = new EntityTagHeaderValue("\"" + etag.ToString() + "\"", true);
            return this.ResponseMessage(response);
        }

        public ResponseMessageResult Ok<T>(T obj, Guid etag)
        {
            var response = this.Request.CreateResponse(HttpStatusCode.OK, obj);
            response.Headers.CacheControl = new CacheControlHeaderValue { NoCache = true };
            response.Headers.ETag = new EntityTagHeaderValue("\"" + etag.ToString() + "\"", true);
            return this.ResponseMessage(response);
        }
    }
}
