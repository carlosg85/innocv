﻿using Carlos.Entities;
using inercya.EntityLite;
using Prueba_Carlos.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Prueba_Carlos.Controllers
{
    public class UsersController : BaseApiController
    {
        [HttpGet]
        [Route("api/user/getall")]
        public IHttpActionResult GetAll()
        {
            var users = this.DataService.UserRepository.Query(Projection.BaseTable).ToList();
            return this.Ok(users);
        }

        [HttpGet]
        [Route("api/user/get/{id}")]
        public IHttpActionResult Get(int id)
        {
            var user = this.DataService.UserRepository.Get(Projection.BaseTable, id);
            return this.Ok(user);
        }

        [HttpPost]
        [ValidateModelStateFilter]
        [Route("api/user/create")]
        public IHttpActionResult Create(User user)
        {
            if (user != null)
            {
                this.DataService.UserRepository.Insert(user);
                return this.Ok(user);
            }
            else
            {
                return this.BadRequest("User cannot be null");
            }
        }

        [HttpPost]
        [ValidateModelStateFilter]
        [Route("api/user/update")]
        public IHttpActionResult Update(User user)
        {
            if (user != null)
            {
                this.DataService.UserRepository.Update(user);
                return this.Ok(user);
            }
            else
            {
                return this.BadRequest("User cannot be null");
            }
        }

        [HttpPost]
        [ValidateModelStateFilter]
        [Route("api/user/remove/{id}")]
        public IHttpActionResult Remove (int id)
        {
            if (id <= 0)
            {
                return this.BadRequest("id must be greather than 0");
            }
            var user = this.DataService.UserRepository.Get(Projection.BaseTable, id);
            if (user != null)
            {
                this.DataService.UserRepository.Delete(id);
                return this.Ok("User deleted sucessfully");
            }
            else
            {
                return this.BadRequest(string.Format("The user with id {0} doesn't exists in database", id));
            }
        }
    }
}
