﻿using System.Collections.Generic;

namespace Prueba_Carlos.Filters
{
    internal class ResponsePackage
    {
        private object content;
        private List<string> modelStateErrors;

        public ResponsePackage(object content, List<string> modelStateErrors)
        {
            this.content = content;
            this.modelStateErrors = modelStateErrors;
        }
    }
}