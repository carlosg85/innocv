﻿using FluentValidation.WebApi;
using Prueba_Carlos.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Prueba_Carlos
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            //config.Filters.Add(new ValidateModelStateFilter());
            //config.MessageHandlers.Add(new ResponseWrappingHandler());

            // Web API routes
            FluentValidationModelValidatorProvider.Configure(config);
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
