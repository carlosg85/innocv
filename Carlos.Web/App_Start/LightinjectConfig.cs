﻿using LightInject;
using LightInject.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Configuration;
using System.IO;

namespace Prueba_Carlos
{
    public static class LightinjectConfig
    {
        public static readonly ServiceContainer Container = new ServiceContainer();

        public static void Register(HttpConfiguration config)
        {
            Container.Register( factory => new Carlos.Entities.CarlosDataService
            {
                
            }, new PerScopeLifetime());
                        
            Container.EnablePerWebRequestScope();
            Container.RegisterApiControllers();
            Container.EnableWebApi(config);
            Container.RegisterControllers();
            Container.EnableMvc();
        }
    }
}