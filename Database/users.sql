﻿CREATE TABLE [dbo].[users]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] VARCHAR(256) NOT NULL, 
    [Birthdate] DATETIME NOT NULL,
	CONSTRAINT uk_users_name UNIQUE (Name)
)
